class PhoneNumber < ApplicationRecord

  def twilio_client
    Twilio::REST::Client.new(TWILIO_SID, TWILIO_TOKEN)
  end

  def send_pin
    twilio_client.messages.create(
      to: phone_number,
      from: ENV['TWILIO_PHONE_NUMBER'],
      body: "Your pin is #{pin}"
    )
  end

  def generate_pin
    self.pin = rand(0000..9999).to_s.rjust(4, "0")
    save
  end

  def verify(entered_pin)
    update(verified: true) if self.pin == entered_pin
  end
end
